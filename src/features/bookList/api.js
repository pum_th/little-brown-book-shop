import { fetch } from '@/utils/api'
import { get } from 'lodash'
import { apiBooks } from '@/config/constants'

export async function getAllBooks(cb) {
  try {
    const booksResponse = await fetch({
      url: apiBooks,
    })
    const books = get(booksResponse, 'data.books', [])
    return cb(books)
  } catch (e) {
    console.log(e)
  }
}
