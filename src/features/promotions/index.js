export {
  default as harryPotterSeriesDiscount,
} from './discount/harryPotterSeries'

export { default as getDiscountFromCartList } from './getDiscountFromCartList'
