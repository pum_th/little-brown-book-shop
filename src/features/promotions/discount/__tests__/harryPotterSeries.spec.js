import harryPotterSeries from '@/features/promotions/discount/harryPotterSeries'

const harryBooks = [
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855652.jpg',
    price: '350',
    title: "Harry Potter and the Philosopher's Stone (I)",
    id: '9781408855652',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855669.jpg',
    price: '350',
    title: 'Harry Potter and the Chamber of Secrets (II)',
    id: '9781408855669',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855676.jpg',
    price: '340',
    title: 'Harry Potter and the Prisoner of Azkaban (III)',
    id: '9781408855676',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855683.jpg',
    price: '360',
    title: 'Harry Potter and the Goblet of Fire (IV)',
    id: '9781408855683',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855690.jpg',
    price: '380',
    title: 'Harry Potter and the Order of the Phoenix (V)',
    id: '9781408855690',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855706.jpg',
    price: '380',
    title: 'Harry Potter and the Half-Blood Prince (VI)',
    id: '9781408855706',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4088/9781408855713.jpg',
    price: '400',
    title: 'Harry Potter and the Deathly Hallows (VII)',
    id: '9781408855713',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4736/9781473659711.jpg',
    price: '120',
    title: 'How to Survive the End of the World ',
    id: '9781473659711',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/5098/9781509809950.jpg',
    price: '160',
    title: 'Solve For Happy',
    id: '9781509809950',
  },
  {
    cover:
      'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/mid/9781/4736/9781473634176.jpg',
    price: '345',
    title: 'The Confidence Project',
    id: '9781473634176',
  },
]

export function getMockBooks(num) {
  return harryBooks.slice(0, num)
}

describe('Harry Unique Potter Series', () => {
  it('Should discount 10%', () => {
    const value = harryPotterSeries(getMockBooks(2))
    expect(value).toBe(70)
  })

  it('Should discount 11%', () => {
    const value = harryPotterSeries(getMockBooks(3))
    expect(value).toBe(114.4)
  })

  it('Should discount 12%', () => {
    const value = harryPotterSeries(getMockBooks(4))
    expect(value).toBe(168)
  })

  it('Should discount 13%', () => {
    const value = harryPotterSeries(getMockBooks(5))
    expect(value).toBe(231.4)
  })

  it('Should discount 14%', () => {
    const value = harryPotterSeries(getMockBooks(6))
    expect(value).toBe(302.4)
  })

  it('Should discount 15%', () => {
    const value = harryPotterSeries(getMockBooks(7))
    expect(value).toBe(384)
  })
})
