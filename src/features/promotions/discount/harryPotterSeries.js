import { get } from 'lodash'
import numeral from 'numeral'

export default function harryPotterSeires(cartProducts) {
  const harryBooksWhiteLists = [
    '9781408855652',
    '9781408855669',
    '9781408855676',
    '9781408855683',
    '9781408855690',
    '9781408855706',
    '9781408855713',
  ]

  const discountPercentageList = {
    2: 0.1,
    3: 0.11,
    4: 0.12,
    5: 0.13,
    6: 0.14,
    7: 0.15,
  }

  const harryBooks = cartProducts.filter(({ id }) => {
    return harryBooksWhiteLists.includes(id)
  })

  const totalPrice = harryBooks.reduce((total, book) => {
    const curBookprice = get(book, 'price', 0)

    return numeral(total)
      .add(curBookprice)
      .value()
  }, 0)

  const discountPercentage = get(discountPercentageList, [harryBooks.length], 0)

  const result = numeral(totalPrice).multiply(discountPercentage)

  return result.value()
}
