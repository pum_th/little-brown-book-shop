import { get, noop } from 'lodash'

import discountList from './discount'

export default function getDiscountFromCartList(cartList) {
  return function getTotalPromotionDiscount(promotionNames = []) {
    const totalPromotionDiscount = promotionNames.reduce((total, proName) => {
      const discountFn = get(discountList, proName, noop)
      return total + discountFn(cartList)
    }, 0)

    return totalPromotionDiscount
  }
}
