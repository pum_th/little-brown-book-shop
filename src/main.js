import Vue from 'vue'
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

import App from '@/components/App.vue'
import router from '@/router'
import store from '@/store'
import { currency } from '@/utils/number'

Vue.config.productionTip = false
Vue.filter('currency', currency)

Vue.use(VueFire)

firebase.initializeApp({
  projectId: 'little-brown-book-shop',
  databaseURL: 'https://little-brown-book-shop.firebaseio.com',
})

export const db = firebase.firestore()

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
