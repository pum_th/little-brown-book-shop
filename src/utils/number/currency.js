import numeral from 'numeral'
import 'numeral/locales/th'

numeral.locale('th')

export default function currency(num) {
  return numeral(num).format('$ 0,0.00')
}
