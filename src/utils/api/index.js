import axios from 'axios'
import { db } from '@/main'

export function fetch({ url, ...options }) {
  return axios({
    url,
    ...options,
  })
}

export async function updateCheckoutStore(data) {
  try {
    await db
      .collection('pos')
      .doc('customer-screen')
      .set(data, { merge: true })
  } catch (error) {
    console.error('Error writing document: ', error)
  }
}
