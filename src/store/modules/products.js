import * as bookListAPI from '@/features/bookList/api'

const state = {
  books: [],
}

const getters = {}

const actions = {
  getAllBooks({ commit }) {
    bookListAPI.getAllBooks(books => {
      commit('setBooks', books)
    })
  },
}

const mutations = {
  setBooks(state, books) {
    state.books = books
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
