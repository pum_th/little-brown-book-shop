import numeral from 'numeral'
import { getDiscountFromCartList } from '@/features/promotions'
import { updateCheckoutStore } from '@/utils/api'

const state = {
  items: [],
  received: '',
  checkoutStatus: '',
}

const getters = {
  cartProducts: state => {
    return state.items.map(item => item)
  },

  cartTotalDiscount: (_, getters) => {
    const getTotalPromotionDiscount = getDiscountFromCartList(
      getters.cartProducts,
    )

    return getTotalPromotionDiscount(['harryPotterSeries'])
  },

  cartTotalPrice: (_, getters) => {
    return getters.cartProducts.reduce((total, { price, quantity }) => {
      return total + price * quantity
    }, 0)
  },

  cartNetPrice: (_, getters) => {
    return getters.cartTotalPrice - getters.cartTotalDiscount
  },

  receivedAmount: state => {
    return state.received
  },

  changeAmount: (state, getters) => {
    const defaultChange = 0
    const received = numeral(state.received)
    const change = received.value() - getters.cartNetPrice

    if (change > 0) {
      return numeral(change).format('0,0.00')
    }

    return defaultChange
  },

  checkoutStatus(state) {
    return state.checkoutStatus
  },
}

const actions = {
  addProductToCart({ state, commit }, product) {
    const cartItem = state.items.find(item => item.id === product.id)

    if (!cartItem) {
      commit('pushProductToCart', product)
    } else {
      commit('updateItemQuantity', { type: 'increment', cartItem })
    }
  },

  decrementProductInCart({ state, commit }, product) {
    const cartItem = state.items.find(item => item.id === product.id)

    if (cartItem.quantity === 1) {
      const index = state.items.findIndex(item => item.id === product.id)
      state.items.splice(index, 1)
    } else {
      commit('updateItemQuantity', { type: 'decrement', cartItem })
    }
  },

  checkout({ commit }) {
    commit('setCartItems', { items: [] })
    commit('setCheckoutStatus', '')
    commit('clearReceived')
  },

  updateCheckoutDB({ state, getters }) {
    updateCheckoutStore({
      items: state.items,
      total: getters.cartTotalPrice,
      discount: getters.cartTotalDiscount,
      net: getters.cartNetPrice,
      received: state.received,
      change: getters.changeAmount,
      checkoutStatus: state.checkoutStatus,
    })
  },
}

const mutations = {
  pushProductToCart(state, product) {
    state.items.push({
      ...product,
      orderPrice: product.price * 1,
      quantity: 1,
    })
  },

  setCartItems(state, { items }) {
    state.items = items
  },

  updateItemQuantity(state, payload) {
    const { type, cartItem } = payload
    const currentItem = state.items.find(item => item.id === cartItem.id)

    if (type === 'increment') {
      currentItem.quantity++
    } else {
      currentItem.quantity--
    }

    currentItem.orderPrice = currentItem.price * currentItem.quantity
  },

  changeReceived(state, num) {
    const digi = state.received.split('').length

    if (digi > 6) return

    state.received = numeral(state.received + num).format('0')
  },

  setReceived(state, num) {
    const receivedState = numeral(state.received)
    const addReceived = numeral(num)
    const result = receivedState.value() + addReceived.value()
    state.received = numeral(result).format('0')
  },

  clearReceived(state) {
    state.received = '0'
  },

  setCheckoutStatus(state, status) {
    state.checkoutStatus = status
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
