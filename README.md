# Little Brown Book Shop

## Setup on local development
- Clone this [repo](https://bitbucket.org/pum_th/little-brown-book-shop/src/master/)
- Install dependencies by running `yarn` or `npm install`
- Start the dev server `npm run serve`
- Open http://localhost:3000

## Demo
Staff screen: https://little-brown-book-shop.firebaseapp.com/

Customer screen: https://little-brown-book-shop.firebaseapp.com/#/customer

## Deployment

### Compiles and minifies for production
```
npm run build

```
### Compiles and minifies for beta environment
```
npm run build:beta
```

### Deploying app into a  rootdirectory
Deploy the built content in ```dist``` folder to your root directory server.

### Deploying app into a  subdirectory
If you want to deploy into a subdirectory on server, you need to edit the ```vue.config.js``` file like this.
```
module.exports = {
  baseUrl: 'https://www.yourdomain.com/subdir/yourapp',
}
```


## Testing

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

## Created by:
Thongchai Kitiyanantawong
